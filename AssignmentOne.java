
import java.util.Scanner;

public class AssignmentOne {
   

    public void CreateRectangle() {
        String horizontal="";
        String vertical = ""; 
        String spaces ="";

        Scanner myScanner = new Scanner(System.in); 
        
        //Setting width
        System.out.println("Enter width: ");
        Integer width = myScanner.nextInt(); 
        if(width < 2) {
            throw new IllegalArgumentException("In order for it to be a rectangle, you need at least 2 tiles");
        }
        
        //Setting height
        System.out.println("Enter height: ");
        Integer height = myScanner.nextInt(); 
        if(height < 2) {
            throw new IllegalArgumentException("In order for it to be a rectangle, you need at least 2 tiles");
        }

        //Setting the symbol you want to draw the rectangle with
        System.out.println("Enter symbol: "); 
        String symbol = myScanner.next();

        //Calculating how many spaces between sides
       int spaceLength = width - 2; 
       for(int x = 0; x < spaceLength; x++) {
         spaces = spaces +" ";  
       }

       for(int i = 0 ; i < width; i++) {      
           horizontal = horizontal + symbol;
       }
        System.out.println(horizontal); 
       
       for(int y = 0; y < height-2; y++) {
        vertical = symbol + spaces + symbol;
        System.out.println(vertical);  

   }
        System.out.println(horizontal); 
    
}

    public static void main(String[] args) {   
        AssignmentOne A1 = new AssignmentOne();
        A1.CreateRectangle(); 
    }

}
